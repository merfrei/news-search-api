News Search API
==============


Searches in few resources at the same time and returns the results as a JSON with the more relevants results.

It is a project made for learning purposes only and to show the use of Go in these types of tasks.
