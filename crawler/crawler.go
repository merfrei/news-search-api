package crawler

import (
	"context"
)

// Result represents a search result
type Result struct {
	Headline string `json:"headline"`
	Subhead  string `json:"subhead"`
	Source   string `json:"source"`
	Time     string `json:"time"`
	URL      string `json:"url"`
}

// Crawler interface
type Crawler interface {
	Name() string
	Search(context.Context, string) <-chan *Result
}
