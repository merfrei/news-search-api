package yahoo

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"gitlab.com/merfrei/news-search-api/crawler"
	ubur "gitlab.com/merfrei/ubur/crawler"
)

const name = "Yahoo News"
const ua = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"

var headers = map[string]string{"User-Agent": ua}

// Crawler extracts results from Bing News page
type Crawler struct {
	name       string
	downloader *ubur.Downloader
}

// New create a new Crawler instance
func New(downloader *ubur.Downloader) *Crawler {
	return &Crawler{
		name:       name,
		downloader: downloader,
	}
}

// Name returns the crawler name
func (crw *Crawler) Name() string {
	return name
}

// Search is the main function for searching and extracting the results
func (crw *Crawler) Search(ctx context.Context, kw string) <-chan *crawler.Result {
	output := make(chan *crawler.Result)
	go func() {
		defer close(output)
		rf := &ubur.RequestsFactory{
			Method: "GET",
			Header: headers,
		}

		url, err := url.Parse("https://news.search.yahoo.com/search")
		if err != nil {
			log.Println("YAHOO: error found, not a valid URL")
			return
		}

		q := url.Query()
		q.Add("p", kw)
		url.RawQuery = q.Encode()

		var wg sync.WaitGroup

		requests := rf.RequestsFromSlide(ctx, "SEARCH", url)
		scheduler := ubur.NewBasicScheduler(crw.downloader.Concurrency, "SEARCH")
		go scheduler.Append(ctx, requests)

		responses := crw.downloader.Download(ctx, &wg, scheduler, "SEARCH")

		results := ubur.Parse(ctx, &wg, parseResults, responses, "SEARCH")

		for {
			select {
			case <-ctx.Done():
				return
			case result, ok := <-results:
				if !ok {
					return
				}
				select {
				case <-ctx.Done():
				case output <- result.(*crawler.Result):
				}
			}
		}
	}()
	return output
}

func parseResults(ctx context.Context, r *http.Response) <-chan interface{} {
	results := make(chan interface{})
	go func() {
		defer close(results)
		defer r.Body.Close()
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [YAHOO]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}
		doc.Find("div.dd.NewsArticle").Each(func(_ int, s *goquery.Selection) {
			title := strings.TrimSpace(s.Find("h4 a").Text())
			subtitle := strings.TrimSpace(s.Find("p.s-desc").Text())
			source := strings.TrimSpace(s.Find("span.s-source").Text())
			time := strings.TrimSpace(s.Find("span.s-time").Text())
			URL := s.Find("h4 a").AttrOr("href", "")
			result := &crawler.Result{
				Headline: title,
				Subhead:  subtitle,
				Source:   source,
				Time:     time,
				URL:      URL,
			}
			select {
			case <-ctx.Done():
			case results <- result:
			}
		})
	}()
	return results
}
