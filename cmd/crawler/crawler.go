package main

import (
	"context"
	"encoding/json"
	"flag"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	_ "gitlab.com/merfrei/news-search-api/internal/downloader"

	"gitlab.com/merfrei/news-search-api/crawler"
	"gitlab.com/merfrei/news-search-api/crawler/bing"
	"gitlab.com/merfrei/news-search-api/crawler/google"
	"gitlab.com/merfrei/news-search-api/crawler/yahoo"
	"gitlab.com/merfrei/news-search-api/internal/downloader"
)

// CrawlResult is a type to link the crawl name to a search result
type CrawlResult struct {
	Name   string
	Result *crawler.Result
}

func runSearch(ctx context.Context, wg *sync.WaitGroup, c crawler.Crawler, kw string, output chan<- CrawlResult) {
	defer wg.Done()
	results := c.Search(ctx, kw)

loop:
	for {
		select {
		case <-ctx.Done():
			log.Println("CANCELED")
			break loop
		case r, ok := <-results:
			if !ok {
				log.Println("DONE")
				break loop
			}
			result := CrawlResult{
				Name:   c.Name(),
				Result: r,
			}
			select {
			case <-ctx.Done():
			case output <- result:
			}
		}
	}
}

func main() {
	var kw string

	flag.StringVar(&kw, "search", "", "Search string")
	flag.Parse()

	if len(kw) < 3 {
		log.Fatal("Search too short")
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	results := make(chan CrawlResult)

	var wg sync.WaitGroup

	googleC := google.New(downloader.Downloader)
	wg.Add(1)
	go runSearch(ctx, &wg, googleC, kw, results)

	bingC := bing.New(downloader.Downloader)
	wg.Add(1)
	go runSearch(ctx, &wg, bingC, kw, results)

	yahooC := yahoo.New(downloader.Downloader)
	wg.Add(1)
	go runSearch(ctx, &wg, yahooC, kw, results)

	output := make(map[string][]*crawler.Result)

	var pip sync.WaitGroup
	pip.Add(1)
	go func() {
		defer pip.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case cr, ok := <-results:
				if !ok {
					return
				}
				output[cr.Name] = append(output[cr.Name], cr.Result)
			}
		}
	}()

	wg.Wait()
	close(results)
	pip.Wait()

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")

	enc.Encode(output)
}
