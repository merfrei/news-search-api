package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/merfrei/news-search-api/crawler"
	"gitlab.com/merfrei/news-search-api/crawler/bing"
	"gitlab.com/merfrei/news-search-api/crawler/google"
	"gitlab.com/merfrei/news-search-api/crawler/yahoo"
	"gitlab.com/merfrei/news-search-api/internal/downloader"
	_ "gitlab.com/merfrei/news-search-api/internal/downloader"

	"github.com/julienschmidt/httprouter"
)

// CrawlResult is a type to link the crawl name to a search result
type CrawlResult struct {
	Name   string
	Result *crawler.Result
}

// Output represents the returned response
type Output struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Total   uint        `json:"total"`
}

func runSearch(ctx context.Context, wg *sync.WaitGroup, c crawler.Crawler, kw string, output chan<- CrawlResult) {
	defer wg.Done()
	results := c.Search(ctx, kw)

loop:
	for {
		select {
		case <-ctx.Done():
			log.Println("CANCELED")
			break loop
		case r, ok := <-results:
			if !ok {
				log.Println("DONE")
				break loop
			}
			result := CrawlResult{
				Name:   c.Name(),
				Result: r,
			}
			select {
			case <-ctx.Done():
			case output <- result:
			}
		}
	}
}

// Search run the crawler and write the results to the reponse
func Search(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	output := Output{}
	kw := ps.ByName("query")

	w.Header().Set("Content-Type", "application/json")

	if len(kw) < 3 {
		output.Status = "error"
		output.Message = "The search term is too short"
		body, err := json.Marshal(output)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(`{"error": "unknown error found"}`))
			return
		}
		w.Write(body)
		return
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	results := make(chan CrawlResult)

	var wg sync.WaitGroup

	googleC := google.New(downloader.Downloader)
	wg.Add(1)
	go runSearch(ctx, &wg, googleC, kw, results)

	bingC := bing.New(downloader.Downloader)
	wg.Add(1)
	go runSearch(ctx, &wg, bingC, kw, results)

	yahooC := yahoo.New(downloader.Downloader)
	wg.Add(1)
	go runSearch(ctx, &wg, yahooC, kw, results)

	data := make(map[string][]*crawler.Result)
	var total uint = 0

	var pip sync.WaitGroup
	pip.Add(1)
	go func() {
		defer pip.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case cr, ok := <-results:
				if !ok {
					return
				}
				data[cr.Name] = append(data[cr.Name], cr.Result)
				total++
			}
		}
	}()

	wg.Wait()
	close(results)
	pip.Wait()

	output.Status = "success"
	output.Message = "All OK!"
	output.Data = data
	output.Total = total

	body, err := json.Marshal(output)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error": "unknown error found"}`))
		return
	}
	w.Write(body)
}

func main() {
	var port uint

	flag.UintVar(&port, "port", 8080, "The port the server will listen")
	flag.Parse()

	router := httprouter.New()
	router.ServeFiles("/website/*filepath", http.Dir("./website"))
	router.GET("/api/search/:query", Search)

	fmt.Println("Listening on port: ", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), router))
}
