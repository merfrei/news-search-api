package downloader

import (
	"net/http"
	"net/url"
	"os"
	"time"

	ubur "gitlab.com/merfrei/ubur/crawler"
)

const maxTry = 3
const concurrency = 1
const timeout = time.Duration(time.Second * 30)

// Downloader to use for all crawlers
var Downloader *ubur.Downloader

func init() {
	Downloader = ubur.NewDownloader(maxTry, concurrency)
	Downloader.Delay = 2
	Downloader.Timeout = timeout

	proxy := os.Getenv("HTTP_PROXY")

	if proxy != "" {
		proxyURL, err := url.Parse(proxy)
		if err == nil {
			Downloader.ProxyFunc = http.ProxyURL(proxyURL)
		}
	}

	Downloader.Init()
}
