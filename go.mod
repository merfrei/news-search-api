module gitlab.com/merfrei/news-search-api

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/pkg/errors v0.9.1
	gitlab.com/merfrei/ubur v0.2.2
)
